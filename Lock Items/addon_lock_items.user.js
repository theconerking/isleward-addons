// ==UserScript==
// @name         Isleward - Lock Items
// @namespace    Isleward.Addon
// @version      1.0.0
// @description  Allows you to lock items so you dont accidentally salvage or destroy them
// @author       Initially created by Polfy, WIP by Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==


(function () {
    var scriptElement = document.createElement( "script" );
    scriptElement.type = "text/javascript";
    scriptElement.src = "https://cdn.statically.io/gl/islewardtribrid/isleward-addons/raw/main/Lock%20Items/addon_lock_items.js";
    document.body.appendChild( scriptElement );

})();
