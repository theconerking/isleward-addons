function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
    (function () {
        addons.register({
            init: function(events) {
                events.on('onGetStats', this.onGetStats.bind(this));
            },
            onGetStats: function(stats) {
                this.hpWarn = document.getElementById('HP-Warning');  
                    if(typeof(this.hpWarn) === 'undefined' || this.hpWarn === null) { 
                        this.hpWarn = $('<div class="HP-Warning">You have LOW HP!</div>')
                            .appendTo($('.ui-container'));                    
                    }

                this.mpWarn = document.getElementById('MP-Warning');
                    if(typeof(this.mpWarn) === 'undefined' || this.mpWarn === null) {
                        this.mpWarn = $('<div class="MP-Warning">You have LOW MP!</div>')
                            .appendTo($('.ui-container'));       
                    }

                if(stats.hp <= ((stats.hpMax / 100) * 30)) {
                    document.getElementById('HP-Warning').style.display = 'block';
                    $('.ui-container').addClass('HP-warning-splash');
                } else {
                    document.getElementById('HP-Warning').style.display = 'none';
                    $('.ui-container').removeClass('HP-warning-splash');
                }
                
                if(stats.mana <= ((stats.manaMax / 100) * 30)) {
                    document.getElementById('MP-Warning').style.display = 'block';
                    $('.ui-container').addClass('MP-warning-splash');
                } else {
                    document.getElementById('MP-Warning').style.display = 'none';
                    $('.ui-container').removeClass('MP-warning-splash');
                }
            }
        });
    })
);
