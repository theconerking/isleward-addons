// ==UserScript==
// @name         Isleward - Show Chat Timestamp
// @namespace    Isleward.Addon
// @version      1.0.0
// @description  Adds a Timestamp infront of each Message
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==


(function () {
    var scriptElement = document.createElement( "script" );
    scriptElement.type = "text/javascript";
    scriptElement.src = "https://cdn.statically.io/gl/islewardtribrid/isleward-addons/raw/main/Show%20Chat%20Timestamp/addon_show_chat_timestamp.js";
    document.body.appendChild( scriptElement );
})();
