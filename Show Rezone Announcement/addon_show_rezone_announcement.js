// ==UserScript==
// @name         Isleward - Show Rezone Announcements
// @namespace    Isleward.Addon
// @version      1.0.0
// @description  Displays a Announcement whenever you entered a area
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==


let zone;
let showAmount = 0;
function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
    (function () {
        addons.register({
            init: function(events) {
                events.on('onGetMap', this.onGetMap.bind(this));
                events.on('onShowCharacterSelect', this.onShowCharacterSelect.bind(this));
            },
            onShowCharacterSelect: function() {
                showAmount = 0;
            },
            onGetMap: function(obj) {
                if (typeof window.showRezoneAnnouncement !== 'undefined') {
                    if(window.showRezoneAnnouncement == false ) {
                        return;
                    }
                }
                showAmount = showAmount + 1;
                if(showAmount < 2) {
                    return;
                }

                if(obj.zoneId === 'cave') {
                    zone = 'Crystal Cave'
                } else
                if(obj.zoneId === 'fjolarok') {
                    zone = 'Fjolarok';
                } else
                if(obj.zoneId === 'estuary') {
                    zone = 'Estuary';
                } else
                if(obj.zoneId === 'fjolgard') {
                    zone = 'Fjolgard';
                } else
                if(obj.zoneId === 'fjolgardHousing') {
                    zone = 'Fjolgard Housing';
                } else
                if(obj.zoneId === 'sewer') {
                    zone = 'Sewer';
                } else {
                    zone = 'Temple Of Gaekatla'
                }

                var uiContainer = $('.ui-container');
                if(!this.rezoneAnn)
                {
                    this.rezoneAnn = $('<div class="rezoneAnnouncement"></div>').appendTo(uiContainer);
                    this.rezoneAnn.css({
                        'width': '75%',
                        'margin': 'auto',
                        'text-align': 'center',
                        'color': '#c0c3cf',
                        'top': '30%',
                        'position': 'absolute',
                        'font-size': '3em',
                        'filter': 'drop-shadow(0 -10px 0 #312136) drop-shadow(0 10px 0 #312136) drop-shadow(10px 0 0 #312136) drop-shadow(-10px 0 0 #312136)'
                    })
                }
                    if($('.rezoneAnnouncement').html().length <= 1){
                        $('.rezoneAnnouncement').html('You entered ' + zone);
        
                    }
                
                    setTimeout(() => {
                    if($('.rezoneAnnouncement').html().length > 0){
                        $('.rezoneAnnouncement').html(' ');
                    }
                }, 2000);
            }
        });
    })
);
