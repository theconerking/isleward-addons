// ==UserScript==
// @name         Isleward - Show Rezone Announcements
// @namespace    Isleward.Addon
// @version      1.0.0
// @description  Displays a Announcement whenever you entered a area
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==


(function () {
    var scriptElement = document.createElement( "script" );
    scriptElement.type = "text/javascript";
    scriptElement.src = "https://cdn.statically.io/gl/islewardtribrid/isleward-addons/raw/main/Show%20Rezone%20Announcement/addon_show_rezone_announcement.js";
    document.body.appendChild( scriptElement );
})();
