// ==UserScript==
// @name         Isleward - Logic for the Radulos Timer
// @namespace    Isleward.Addon
// @version      1.0.0
// @description  Adds the Logic for the Radulos Timer
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==


function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
(function () {
    addons.register({
    init: function(events) {
        events.on('onGetObject', this.onGetObject.bind(this));
    },
    onGetObject: function(obj) {

        if(obj.name === "Radulos"){
            window.bosses[1][7] = obj.id;
            if(typeof window.bosses[1][2] === "undefined" && typeof window.bosses[1][3] !== "undefined"){
                window.bosses[1][2] = new Date();
            }
        }
		if (typeof window.bosses[1][7] != "undefined" && obj.id == window.bosses[1][7] && obj.destroyed){
            if(typeof window.bosses[1][3] === "undefined" && typeof window.bosses[1][2] === "undefined"){
                window.bosses[1][3] = new Date();
            }
			window.bosses[1][5] = 600;
            if(typeof window.bosses[1][2] != "undefined" && typeof window.bosses[1][3] != "undefined"){
                window.bosses[1][5] = 600;
            }
		}
    },
});
    var repeat = function(){
        if(typeof window.bosses[1][5] === "undefined"){
            window.bosses[1][5] = 0;
        }
        if(window.bosses[1][5] > 0){
            window.bosses[1][5]--;
        }
        var toHHMMSS = (secs) => {
            var sec_num = parseInt(secs, 10);
            var hours = Math.floor(sec_num / 3600) % 24;
            var minutes = Math.floor(sec_num / 60) % 60;
            var seconds = sec_num % 60;
            return [hours,minutes,seconds]
                .map(v => v < 10 ? "0" + v : v)
                .filter((v,i) => v !== "00" || i > 0)
                .join(":")
        }

        window.bosses[1][6] = 'Respawns in: ' + toHHMMSS(window.bosses[1][5]);

        if(window.bosses[1][5] == 597) {
            window.bosses[1][4]++;
            jQuery('<div class="list-message color-redA chat CombatLog">' + window.bosses[1][4]+' Radulos Killed' + '</div>').appendTo(jQuery(".uiMessages .list"));
            jQuery(".uiMessages .list").scrollTop(9999999);
        }

        $('.ui-container .addon-Left .addon-Timers .addon-Timers-List .addon-rad .addon-rad-Description').html(window.bosses[1][6])

    };
    setInterval(repeat, 1000);
}));
