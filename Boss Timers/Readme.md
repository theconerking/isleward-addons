# Boss Timers

This addon adds a Bosstimer panel to the left side of the screen, it displays the respawn time.
![''](image2.png)

Additionally it also sends a message into the chat after every bosskill.
![''](image1.png)
