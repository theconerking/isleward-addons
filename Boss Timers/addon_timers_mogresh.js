// ==UserScript==
// @name         Isleward - Logic for the Mogresh Timer
// @namespace    Isleward.Addon
// @version      1.0.0
// @description  Adds the Logic for the Mogresh Timer
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==


function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
(function () {
    addons.register({
    init: function(events) {
        events.on('onGetObject', this.onGetObject.bind(this));
    },
    onGetObject: function(obj) {

        if(obj.name === "m'ogresh"){
            window.bosses[0][7] = obj.id;
            if(typeof window.bosses[0][2] === "undefined" && typeof window.bosses[0][3] !== "undefined"){
                window.bosses[0][2] = new Date();
            }
        }
		if (typeof window.bosses[0][7] != "undefined" && obj.id == window.bosses[0][7] && obj.destroyed){
            if(typeof window.bosses[0][3] === "undefined" && typeof window.bosses[0][2] === "undefined"){
                window.bosses[0][3] = new Date();
            }
			window.bosses[0][5] = 139;
            if(typeof window.bosses[0][2] != "undefined" && typeof window.bosses[0][3] != "undefined"){
                window.bosses[0][5] = 139;
            }
		}
    },
});
    var repeat = function(){
        if(typeof window.bosses[0][5] === "undefined"){
            window.bosses[0][5] = 0;
        }
        if(window.bosses[0][5] > 0){
            window.bosses[0][5]--;
        }
        var toHHMMSS = (secs) => {
            var sec_num = parseInt(secs, 10);
            var hours = Math.floor(sec_num / 3600) % 24;
            var minutes = Math.floor(sec_num / 60) % 60;
            var seconds = sec_num % 60;
            return [hours,minutes,seconds]
                .map(v => v < 10 ? "0" + v : v)
                .filter((v,i) => v !== "00" || i > 0)
                .join(":")
        }

        window.bosses[0][6] = 'Respawns in: ' + toHHMMSS(window.bosses[0][5]);

        if(window.bosses[0][5] == 138) {
            window.bosses[0][4]++
            jQuery('<div class="list-message color-redA chat CombatLog">' + window.bosses[0][4]+' Mogresh Killed' + '</div>').appendTo(jQuery(".uiMessages .list"));
            jQuery(".uiMessages .list").scrollTop(9999999);
        }

        $('.ui-container .addon-Left .addon-Timers .addon-Timers-List .addon-mog .addon-mog-Description').html(window.bosses[0][6])

    };
    setInterval(repeat, 1000);
}));
