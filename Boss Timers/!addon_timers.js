// ==UserScript==
// @name         Isleward - Timer UI
// @namespace    Isleward.Addon
// @version      1.1.0
// @description  Adds the UI for the Bosstimers
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==


const colors2 =[
    ['#48edff'],
    ['#3fa7dd'],
    ['#505360'],
    ['#373041']
];
const timers = [ // last respawned - last killed - kills - respawntime - respawntimetoHHMMSS - id
    ['mog', 'Mogresh',0, 0, 0, 0, 0, 0],
    ['rad', 'Radulos',0, 0, 0, 0, 0, 0],
    ['stink', 'Stinktooth', 0, 0, 0, 0, 0, 0],
    ['thumper', 'Thumper', 0, 0, 0, 0, 0, 0],
    ['sund', 'Sundfehr', 0, 0, 0, 0, 0, 0]
]
window.bosses = timers;

function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
    (function () {
        addons.register({
            init: function(events) {
                events.on('onGetPlayer', this.onGetPlayer.bind(this));
                events.on('onGetObject', this.onGetObject.bind(this));

            },
            onGetPlayer: function() {
                if (typeof(document.getElementById('addon-Left')) === 'undefined' || document.getElementById('addon-Left') === null) appendTimerPage();
                
                function appendTimerPage() {

                    this.container = $('.ui-container');
    
                    
                    this.left = $('<div id = "addon-Left" class="addon-Left"></div>')
                        .appendTo(this.container);
                        
                    this.left.css({
                        'position': 'absolute',
                        'left': '10px',
                        'top': '94px'
                    });
    
                
                    this.addonTimers = $('<div id = "addon-Timers" class="addon-Timers"></div>')
                        .appendTo(this.left);
                        
                    this.addonTimers.css({
                        'margin-top' : '10px',
                        'width' : '325px',
                        'visibility' : 'visible',
                        'box-shadow' : '0 -2px 0 #2d2136,0 2px 0 #2d2136,2px 0 0 #2d2136,-2px 0 0 #2d2136'
                    });
        
                    this.addonTimersHeading = $('<div id = "addon-Timers-Heading" class="addon-Timers-Heading">Bosses</div>')
                        .appendTo(this.addonTimers);
        
                    this.addonTimersHeading.css({
                        'color' : '#ffeb38',
                        'padding' : '8px',
                        'background-color' : 'rgba(58,59,74,.9)'
                    });
        
                    this.addonTimersList = $('<div id = "addon-Timers-List" class="addon-Timers-List"></div>')
                        .appendTo(this.addonTimers);
                }
    
                timers.forEach(timer=> {
                    if(document.getElementById(`addon-${timer[0]}`)){
                        return;
                    }
                this.left = $('.ui-container> .addon-Left .addon-Timers .addon-Timers-List');

                this.timer = $(`<div id = "addon-${timer[0]}" class="addon-${timer[0]}"></div>`).appendTo(this.left);
                this.timer.css({
                    'cursor' : 'pointer',
                    'padding' : '8px',
                    'background-color' : 'rgba(58,59,74,.9)',
                    'color' : '#fcfcfc'
                });

                this.timerName = $(`<div id = "addon-${timer[0]}-Name" class="addon-${timer[0]}-Name">${timer[1]}</div>`).appendTo(this.timer);
                this.timerName.css({
                    'margin-bottom' : '3px',
                    'color' : '#3fa7dd'
                });

                this.timerDesc = $(`<div id = "addon-${timer[0]}-Description" class="addon-${timer[0]}-Description">Fetching...</div>`).appendTo(this.timer);
                this.timerDesc.css({
                    'color' : '#a3a3a3'
                });

                document.getElementById(`addon-${timer[0]}`).addEventListener('mouseover', function() {
                    document.getElementById(`addon-${timer[0]}`).style.backgroundColor = 'rgba(92,93,117,.9)';
                    document.getElementById(`addon-${timer[0]}-Description`).style.color = '#44cb95';

                });
                document.getElementById(`addon-${timer[0]}`).addEventListener('mouseout', function() {
                    document.getElementById(`addon-${timer[0]}`).style.backgroundColor = 'rgba(58,59,74,.9)';
                    document.getElementById(`addon-${timer[0]}-Description`).style.color = '#a3a3a3';
                });
                

                });
                
            },
            onGetObject: function() {

                if (typeof window.showTimers !== 'undefined') {
                    if(window.showTimers == false ) {
                        if(document.getElementById('addon-Timers').style.display == 'none') {
                            return;
                        } else {
                            document.getElementById('addon-Timers').style.display = 'none';
                        }
                    } else
                    if(window.showTimers == true ) {
                        if(document.getElementById('addon-Timers').style.display == 'block') {
                            return;
                        } else {
                            document.getElementById('addon-Timers').style.display = 'block';
                        }
                    }
                }
            }
        });
    })
);