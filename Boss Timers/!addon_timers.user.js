// ==UserScript==
// @name         Isleward - Bosstimer
// @namespace    Isleward.Addon
// @version      1.0.0
// @description  Adds the UI and logic for the Bosstimers
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==


(function () {
    var scriptElement = document.createElement( "script" );
    scriptElement.type = "text/javascript";
    scriptElement.src = "https://cdn.statically.io/gl/islewardtribrid/isleward-addons/raw/main/Boss%20Timers/!addon_timers.js";
    document.body.appendChild( scriptElement );

    var scriptElementmog = document.createElement( "script" );
    scriptElementmog.type = "text/javascript";
    scriptElementmog.src = "https://cdn.statically.io/gl/islewardtribrid/isleward-addons/raw/main/Boss%20Timers/addon_timers_mogresh.js";
    document.body.appendChild( scriptElementmog );
    var scriptElementrad = document.createElement( "script" );
    scriptElementrad.type = "text/javascript";
    scriptElementrad.src = "https://cdn.statically.io/gl/islewardtribrid/isleward-addons/raw/main/Boss%20Timers/addon_timers_radulos.js";
    document.body.appendChild( scriptElementrad );
    var scriptElementstink = document.createElement( "script" );
    scriptElementstink.type = "text/javascript";
    scriptElementstink.src = "https://cdn.statically.io/gl/islewardtribrid/isleward-addons/raw/main/Boss%20Timers/addon_timers_stinktooth.js";
    document.body.appendChild( scriptElementstink );
})();
