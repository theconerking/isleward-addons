// ==UserScript==
// @name         Isleward - Restyled Status HUD
// @namespace    Isleward.Addon
// @version      1.0.0
// @description  Changes the size of the Status Hud, position of quickslot and target and adds more stats to the Status HUD
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==


(function () {
    var scriptElement = document.createElement( "script" );
    scriptElement.type = "text/javascript";
    scriptElement.src = "https://cdn.statically.io/gl/islewardtribrid/isleward-addons/raw/main/Restyled%20Status%20HUD/addon_restyled_status_hud.js";
    document.body.appendChild( scriptElement );
})();
