// ==UserScript==
// @name         Isleward - Show Annoumcements
// @namespace    Isleward.Addon
// @version      1.0.0
// @description  Shows announcements regarding Plague of rats, ingar restocking and finn appearing
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==


(function () {
    var scriptElement = document.createElement( "script" );
    scriptElement.type = "text/javascript";
    scriptElement.src = "https://cdn.statically.io/gl/islewardtribrid/isleward-addons/raw/main/Show%20Announcements/addon_show_announcements.js";
    document.body.appendChild( scriptElement );
})();
