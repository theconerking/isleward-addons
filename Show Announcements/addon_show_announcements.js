// ==UserScript==
// @name         Isleward - Show Annoumcements
// @namespace    Isleward.Addon
// @version      1.0.0
// @description  Shows announcements regarding Plague of rats, ingar restocking and finn appearing
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==


function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { 
            defer(method) 
        }, 50);
    }
}
defer(
    (function () {
        addons.register({
            init: function(events) {
            }, 
        });
        
        let repeat = function() {

            let [timeHours, timeMinutes, timeSeconds] = [new Date().getUTCHours(), new Date().getUTCMinutes(), new Date().getUTCSeconds()];
            let finnHours = [20, 0, 4, 8, 12, 16];
            let plagueHours = [23, 2, 5, 8, 11, 14, 17, 20];
            let ingarHours = [23, 0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22];

            if (!this.announcement) {

                this.uiContainer = $('.ui-container');

                this.announcement = $('<div class="announcement"></div>').appendTo(this.uiContainer);
                this.announcement.css({
                    'width': '75%',
                    'margin': 'auto',
                    'text-align': 'center',
                    'color': '#4ac441',
                    'top': '30%',
                    'position': 'absolute',
                    'font-size': '3em',
                    'filter': 'drop-shadow(0 -10px 0 #312136) drop-shadow(0 10px 0 #312136) drop-shadow(10px 0 0 #312136) drop-shadow(-10px 0 0 #312136)'
                })
            }
            if (typeof window.showFinnAnnouncement !== 'undefined') {
                if(window.showFinnAnnouncement == false ) {
                    return;
                } else 
                {
                    if(finnHours.includes(timeHours) && timeMinutes == 57 && timeSeconds <= 10) {
                        if ($('.announcement').html().length <= 1) {
                            $('.announcement').html('Finn Appears in 3 Minutes!');  
                        }  
                    } else 
                    if ($('.announcement').html().length > 0) {
                        $('.announcement').html(' ');
                    }
                }
            }
            if (typeof window.showPlagueAnnouncement !== 'undefined') {
                if(window.showPlagueAnnouncement == false ) {
                    return;
                } else
                {
                    if(plagueHours.includes(timeHours) && timeMinutes == 57 && timeSeconds <= 10) {
                        if ($('.announcement').html().length <= 1) {
                            $('.announcement').html('Plague of Rats starts in 3 Minutes!');  
                        }  
                    } else 
                    if ($('.announcement').html().length > 0) {
                        $('.announcement').html(' ');
                    }
                }
            }
            if (typeof window.showIngarAnnouncement !== 'undefined') {
                if(window.showIngarAnnouncement == false ) {
                    return;
                } else
                {
                    if(ingarHours.includes(timeHours) && timeMinutes == 57 && timeSeconds <= 10) {
                        if ($('.announcement').html().length <= 1) {
                            $('.announcement').html('Ingar Restocks in 3 Minutes!');  
                        }  
                    } else
                    if(ingarHours.includes(timeHours) && timeMinutes == 27 && timeSeconds <= 10) {
                        if ($('.announcement').html().length <= 1) {
                            $('.announcement').html('Ingar Restocks in 3 Minutes!');  
                        }  
                    } else 
                    if ($('.announcement').html().length > 0) {
                        $('.announcement').html(' ');
                    }
                }
            }

            
        };
        setInterval(repeat, 1000);
    })
);
