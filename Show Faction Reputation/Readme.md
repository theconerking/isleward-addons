# Show Faction Reputation

Displays the amount of Reputation you have for each faction in the faction menu.

![''](image.png)
