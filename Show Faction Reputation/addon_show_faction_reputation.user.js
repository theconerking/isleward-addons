// ==UserScript==
// @name         Isleward - Show Faction Reputation
// @namespace    Isleward.Addon
// @version      1.0.0
// @description  Displays the amount of Reputation you have for each faction in the faction menu
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==


(function () {
    var scriptElement = document.createElement( "script" );
    scriptElement.type = "text/javascript";
    scriptElement.src = "https://cdn.statically.io/gl/islewardtribrid/isleward-addons/raw/main/Show%20Faction%20Reputation/addon_show_faction_reputation.js";
    document.body.appendChild( scriptElement );
})();
