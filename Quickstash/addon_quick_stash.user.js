// ==UserScript==
// @name         Isleward - Quick Stash
// @namespace    Isleward.Addon
// @version      1.0.0
// @description  Allows players to quickly stash items by hovering over the item and pressing b
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==


(function () {
    var scriptElement = document.createElement( "script" );
    scriptElement.type = "text/javascript";
    scriptElement.src = "https://cdn.statically.io/gl/islewardtribrid/isleward-addons/raw/main/Quickstash/addon_quick_stash.js";
    document.body.appendChild( scriptElement );

})();
