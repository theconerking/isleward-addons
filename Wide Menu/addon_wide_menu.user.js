// ==UserScript==
// @name         Isleward - Wide Menu
// @namespace    Isleward.Addon
// @version      1.0.0
// @description  Changes the size of the menu
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==


(function () {
    var scriptElement = document.createElement( "script" );
    scriptElement.type = "text/javascript";
    scriptElement.src = "https://cdn.statically.io/gl/islewardtribrid/isleward-addons/raw/main/Wide%20Menu/addon_wide_menu.js";
    document.body.appendChild( scriptElement );
})();
