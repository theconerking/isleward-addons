
// ==UserScript==
// @name         Isleward - Wide Menu
// @namespace    Isleward.Addon
// @version      1.0.0
// @description  Changes the size of the menu
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==


function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
    (function () {
        addons.register({
            init: function(events) {
                events.on('onGetStats', this.onGetStats.bind(this));
            },
            onGetStats: function(obj) {
                $('.ui-container .uiMenu').css({
                    'width': '672',
                    'height': '72'
                })
            }
        })    
    })
);