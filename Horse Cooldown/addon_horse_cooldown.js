// ==UserScript==
// @name         Isleward - Show Horse Cooldown
// @namespace    Isleward.Addon
// @version      1.0.0
// @description  Displays the Cooldown of the Horse
// @author       Initially created by Disasm, broken only into the cd addon by Tribrid
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==


function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
    (function () {
        addons.register({
            init: function(events) {
                events.on('onGetItems', this.onGetItems.bind(this));
            },
            onGetItems: function(obj) {
                for (let item_id in obj) {
                    item = obj[item_id]
                    if ('quickSlot' in item) {
                        $(".quickItem .info").text("R " + item.cd);
                        break
                    }
                }
            }
        });
    })
);
