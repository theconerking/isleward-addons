// ==UserScript==
// @name         Isleward - Show Horse Cooldown
// @namespace    Isleward.Addon
// @version      1.0.0
// @description  Displays the Cooldown of the Horse
// @author       Initially created by Disasm, broken only into the cd addon by Tribrod
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==


(function () {
    var scriptElement = document.createElement( "script" );
    scriptElement.type = "text/javascript";
    scriptElement.src = "https://cdn.statically.io/gl/islewardtribrid/isleward-addons/raw/main/Horse%20Cooldown/addon_horse_cooldown.js";
    document.body.appendChild( scriptElement );
})();
