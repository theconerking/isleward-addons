// ==UserScript==
// @name         Isleward - Player Name Color
// @namespace    Isleward.Addon
// @version      1.0.0
// @description  Changes the player name color clientside
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==

(function () {
    var scriptElement = document.createElement( "script" );
    scriptElement.type = "text/javascript";
    scriptElement.src = "https://cdn.statically.io/gl/islewardtribrid/isleward-addons/raw/main/Custom%20Name%20Color/addon_player_name_color.js";
    document.body.appendChild( scriptElement );
})();

