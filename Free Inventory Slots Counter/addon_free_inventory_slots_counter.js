// ==UserScript==
// @name         Isleward - Free Inventory Slots Counter
// @namespace    Isleward.Addon
// @version      1.0.0
// @description  Shows the number of Free Inventory Slots
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==


function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
    (function () {
        addons.register({
            init: function(events) {
                events.on('onGetObject', this.onGetObject.bind(this));
            },
            onGetObject: function(obj) {
                if(!obj) {
                    return
                }
                if(!window){
                    return;
                }
                if(!window.player) {
                    return
                }
                if(!window.player.inventory) {
                    return
                }

                let x = 0;
                player.inventory.items.forEach(element => {
                    if(element.eq == true) {
                      x =x+1;  
                    }
                });
                if($('.ui-container .uiMenu .btnInventory .icon')[0] == undefined) {
                    return;
                }
                $('.ui-container .uiMenu .btnInventory .icon')[0].innerText = 50-(window.player.inventory.items.length-x);
                $('.ui-container .uiMenu .btnInventory .icon').css({
                    'color':'#fcfcfc'
                })
              
            }
        });
    })
);
