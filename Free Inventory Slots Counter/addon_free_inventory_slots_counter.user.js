// ==UserScript==
// @name         Isleward - Free Inventory Slots Counter
// @namespace    Isleward.Addon
// @version      1.0.0
// @description  Shows the number of Free Inventory Slots
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==


(function () {
    var scriptElement = document.createElement( "script" );
    scriptElement.type = "text/javascript";
    scriptElement.src = "https://cdn.statically.io/gl/islewardtribrid/isleward-addons/raw/main/Free%20Inventory%20Slots%20Counter/addon_free_inventory_slots_counter.js";
    document.body.appendChild( scriptElement );
})();


