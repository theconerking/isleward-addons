# Additional Settings

This Addon adds settings to the menu, those settings straight up work with all the following addons:
- Hide Login/Logoff Messages
- Show Aggressive Entities
- Show Chat Timestamp
- Show Faction Reputation
- Show Player Prophecy
- Show Questmobs
- Show Rezone Announcement
- Boss Timers
- Show Plague Announcement
- Show Ingar Announcement
- Show Finn Announcement
- Show Coordinates

When having the Settings addon, all of the addons names are toggleable, when you have those addons and do NOT have the settings, the addons are automatically active.

![''](image.png)
