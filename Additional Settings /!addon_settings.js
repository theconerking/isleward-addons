// ==UserScript==
// @name         Isleward - Additional Settings
// @namespace    Isleward.Addon
// @version      1.0.0
// @description  Adds Additional Settings to the menu, working with all my other addons
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==


const buttons = [
    ['showCombatLog', 'Show Combat Log', false, 'left'],
    ['showProphecy', 'Show Player Prophecy', false, 'left'],
    ['showAggressive', 'Show Aggressive Mobs', false, 'left'],
    ['showQuestmob', 'Show Questmobs', false, 'left'],
    ['showRezoneAnnouncement', 'Show Rezone Announcement', false, 'left'],
    ['showLoginLogoff', 'Show Login/Logoff Messages', false, 'left'],
    ['showTimestamp', 'Show Chat Timestamp', false, 'left'],
    ['showTimers', 'Show Timers', false, 'left'],
    ['showPlagueAnnouncement', 'Show Plague Announcement', false, 'left'],
    ['showFinnAnnouncement', 'Show Finn Announcement', false, 'left'],
    ['showIngarAnnouncement', 'Show Ingar Announcement', false, 'left'],
    ['showReputation', 'Show Faction Reputation', false, 'left'],
    ['showCoordinates', 'Show Coordinates', false, 'left']
]

function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
    (function () {
        addons.register({
            init: function(events) {
                events.on('onOpenOptions', this.onOpenOptions.bind(this));
                events.on('onCloseOptions', this.onCloseOptions.bind(this));
                events.on('onGetPlayer', this.onGetPlayer.bind(this));
            },
            onOpenOptions: function() {
                document.getElementById('addon-Options-Left').style.display = 'flex';
                document.getElementById('addon-Options-Right').style.display = 'flex';
            },
            onCloseOptions: function() {
                document.getElementById('addon-Options-Left').style.display = 'none';
                document.getElementById('addon-Options-Right').style.display = 'none';
            },
            onGetPlayer: function() {
                if (typeof(document.getElementById('addon-Options-Left')) === 'undefined' || document.getElementById('addon-Options-Left') === null) appendAddonSettingsPageLeft();
                if (typeof(document.getElementById('addon-Options-Right')) === 'undefined' || document.getElementById('addon-Options-Right') === null) appendAddonSettingsPageRight();
                
                function appendAddonSettingsPageLeft() {
                
                    this.container = $('.ui-container');
        
                    this.addonOptionsLeft = $('<div id = "addon-Options-Left" class="addon-Options-Left"></div>')
                        .appendTo(this.container);
        
                    this.addonOptionsLeft.css({
                        'position': 'absolute',
                        'left': '200px',
                        'top': '294px',
                        'display' : 'none',
                        'width': '400px',
                        'background-color': '#373041',
                        'border': '5px solid #3c3f4c',
                        'flex-direction': 'column'
                    });
        
                    this.addonOptionsLeftHeading = $('<div id = "addon-Options-Left-Heading" class="addon-Options-Left-Heading"></div>')
                        .appendTo(this.addonOptionsLeft);
        
                    this.addonOptionsLeftHeading.css({
                        'color' : '#48edff',
                        'width' : '100%',
                        'height' : '36px',
                        'background-color' : '#3c3f4c',
                        'position' : 'relative',
                        'text-align' : 'center'
                    });
        
                    this.addonOptionsLeftHeadingText = $('<div id = "addon-Options-Left-Heading-Text" class="addon-Options-Left-Heading-Text">Addon Options</div>')
                        .appendTo(this.addonOptionsLeftHeading);
        
                    this.addonOptionsLeftHeadingText.css({
                        'padding-top' : '8px',
                        'margin' : 'auto' 
                    });
        
                    this.addonOptionsLeftBottom = $('<div id = "addon-Options-Left-Bottom" class="addon-Options-Left-Bottom"></div>')
                        .appendTo(this.addonOptionsLeft);
        
                    this.addonOptionsLeftBottom.css({
                        'padding': '10px',
                        'height': '100%',
                        'overflow-y': 'auto'
                    });
                    this.addonOptionsLeftBottomList = $('<div id = "addon-Options-Left-Bottom-List" class="addon-Options-Left-Bottom-List"></div>')
                        .appendTo(this.addonOptionsLeftBottom);
        
                    this.addonOptionsLeftBottomList.css({
                        'display': 'flex',
                        'flex-direction': 'column'
                    });
        
                };
        
                function appendAddonSettingsPageRight() {
                    
                    this.container = $('.ui-container');
        
                    this.addonOptionsRight = $('<div id = "addon-Options-Right" class="addon-Options-Right"></div>')
                        .appendTo(this.container);
        
                    this.addonOptionsRight.css({
                        'position': 'absolute',
                        'right': '200px',
                        'top': '294px',
                        'display' : 'none',
                        'width': '400px',
                        'background-color': '#373041',
                        'border': '5px solid #3c3f4c',
                        'flex-direction': 'column'
                    });
        
                    this.addonOptionsRightHeading = $('<div id = "addon-Options-Right-Heading" class="addon-Options-Right-Heading"></div>')
                        .appendTo(this.addonOptionsRight);
        
                    this.addonOptionsRightHeading.css({
                        'color' : '#48edff',
                        'width' : '100%',
                        'height' : '36px',
                        'background-color' : '#3c3f4c',
                        'position' : 'relative',
                        'text-align' : 'center'
                    });
        
                    this.addonOptionsRightHeadingText = $('<div id = "addon-Options-Right-Heading-Text" class="addon-Options-Right-Heading-Text">Addon Options</div>')
                        .appendTo(this.addonOptionsRightHeading);
        
                    this.addonOptionsRightHeadingText.css({
                        'padding-top' : '8px',
                        'margin' : 'auto' 
                    });
        
                    this.addonOptionsRightBottom = $('<div id = "addon-Options-Right-Bottom" class="addon-Options-Right-Bottom"></div>')
                        .appendTo(this.addonOptionsRight);
        
                    this.addonOptionsRightBottom.css({
                        'padding': '10px',
                        'height': '100%',
                        'overflow-y': 'auto'
                    });
                    this.addonOptionsRightBottomList = $('<div id = "addon-Options-Right-Bottom-List" class="addon-Options-Right-Bottom-List"></div>')
                        .appendTo(this.addonOptionsRightBottom);
        
                    this.addonOptionsRightBottomList.css({
                        'display': 'flex',
                        'flex-direction': 'column'
                    });
        
                };

                buttons.forEach(button=> {
                    if(document.getElementById(`addon-${button[0]}`)){
                        return;
                    }

                    window[button[0]] = button[2];
        
                    this.addonOptionsLeft = $('.ui-container> .addon-Options-Left .addon-Options-Left-Bottom .addon-Options-Left-Bottom-List');
                    this.addonOptionsRight = $('.ui-container> .addon-Options-Right .addon-Options-Right-Bottom .addon-Options-Right-Bottom-List');

                    if(button[3] === 'left') {
                        this.option = $(`<div id = "addon-${button[0]}" class="addon-${button[0]}"></div>`).appendTo(this.addonOptionsLeft);
                        this.option.css({
                            'height' : '30px',
                            'display' : 'flex',
                            'justify-content' : 'space-between',
                            'align-items' : 'center'
                        });
                    } else 
                    if(button[3] === 'right') {
                        this.option = $(`<div id = "addon-${button[0]}" class="addon-${button[0]}"></div>`).appendTo(this.addonOptionsRight);
                        this.option.css({
                            'height' : '30px',
                            'display' : 'flex',
                            'justify-content' : 'space-between',
                            'align-items' : 'center'
                        });
                    }

                    

                    this.optionName = $(`<div id = "addon-${button[0]}-Name" class="addon-${button[0]}-Name">${button[1]}</div>`).appendTo(this.option);
                    this.optionName.css({
                        'color': '#3fa7dd',
                        'flex' : '1',
                        'height' : '100%',
                        'display' : 'flex',
                        'align-items' : 'center',
                        'padding' : '0 10px',
                        'margin-right' : '10px',
                        'cursor' : 'pointer'
                    });
                    this.optionValue = $(`<div id = "addon-${button[0]}-Value" class="addon-${button[0]}-Value">${button[2]}</div>`).appendTo(this.option);
                    this.optionValue.css({
                        'color': '#fcfcfc',
                        'display' : 'flex',
                        'align-items' : 'center',
                        'justify-content' : 'flex-end',
                        'padding-right' : '10px'
                    });
                                              
                        document.getElementById(`addon-${button[0]}-Name`).addEventListener('mouseover', function() {
                            document.getElementById(`addon-${button[0]}-Name`).style.backgroundColor = '#505360';
                            document.getElementById(`addon-${button[0]}-Name`).style.color = '#48edff';
                        });
                        document.getElementById(`addon-${button[0]}-Name`).addEventListener('mouseout', function() {
                            document.getElementById(`addon-${button[0]}-Name`).style.backgroundColor = '#373041';
                            document.getElementById(`addon-${button[0]}-Name`).style.color = '#3fa7dd';
                        });
                        document.getElementById(`addon-${button[0]}-Name`).addEventListener('click', function() {
                            switchButtonState(button);
                            
                        });
                });
                function switchButtonState(obj) {
                    if(obj[2] === true) {
                        obj[2] = false;
                    } else
                    if(obj[2] === false) {
                        obj[2] = true;
                    }
                    if(obj[3] === 'left') {
                        $(`.ui-container> .addon-Options-Left .addon-Options-Left-Bottom .addon-Options-Left-Bottom-List .addon-${obj[0]}-Value`).html(obj[2]+'');
                    } else
                    if(obj[3] === 'right') {
                        $(`.ui-container> .addon-Options-Right .addon-Options-Right-Bottom .addon-Options-Right-Bottom-List .addon-${obj[0]}-Value`).html(obj[2]+'');
                    }
                         
                    window[obj[0]] = obj[2];     
                }


            },
        });

        

    })
);
