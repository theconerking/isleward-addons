// ==UserScript==
// @name         Isleward - Additional Settings
// @namespace    Isleward.Addon
// @version      1.0.0
// @description  Adds Additional Settings to the menu, working with all my other addons
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==

(function () {
    var scriptElement = document.createElement( "script" );
    scriptElement.type = "text/javascript";
    scriptElement.src = "https://cdn.statically.io/gl/islewardtribrid/isleward-addons/raw/main/Additional%20Settings%20/!addon_settings.js?ref_type=heads";
    document.body.appendChild( scriptElement );
})();
