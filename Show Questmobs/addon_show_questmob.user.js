// ==UserScript==
// @name         Isleward - Show Questmobs
// @namespace    Isleward.Addon
// @version      1.0.0
// @description  Displays Whether or not a enemy is required for a quest
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==


(function () {
    var scriptElement = document.createElement( "script" );
    scriptElement.type = "text/javascript";
    scriptElement.src = "https://cdn.statically.io/gl/islewardtribrid/isleward-addons/raw/main/Show%20Questmobs/addon_show_questmob.js";
    document.body.appendChild( scriptElement );
})();
