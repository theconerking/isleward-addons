// ==UserScript==
// @name         Isleward - Show Questmobs
// @namespace    Isleward.Addon
// @version      1.0.0
// @description  Displays Whether or not a enemy is required for a quest
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==


function defer(method) {
    if (window.jQuery) {
        method();
    } else {
        setTimeout(function() { defer(method) }, 50);
    }
}
defer(
    (function () {
        addons.register({
            init: function(events) {
                events.on('onGetObject', this.onGetObject.bind(this));
            },
            onGetObject: function(obj) {
                if (typeof window.showQuestmob !== 'undefined') {
                    if(window.showQuestmob == false ) {
                        return;
                    }
                }
                if(!obj) {
                    return
                }
                if(obj.sheetName != 'mobs') {
                    return;
                }
                window.player.quests.quests.forEach(element => {
                    if(element.type == 'killX' || element.type == 'lootGen') {
                        if(obj.name.toLowerCase() == element.mobName.toLowerCase()) {          
                            obj.name = obj.name +'\n' + '- Quest -';
                        }               
                    }
                });
            }
        });
    })
);
