// ==UserScript==
// @name         Isleward - Show Login/Logoff
// @namespace    Isleward.Addon
// @version      1.0.0
// @description  When Active it removes login and logoff messages from the chat
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==


(function () {
    var scriptElement = document.createElement( "script" );
    scriptElement.type = "text/javascript";
    scriptElement.src = "https://cdn.statically.io/gl/islewardtribrid/isleward-addons/raw/main/Hide%20Login-Logoff%20Messages/addon_hide_login-logoff.js";
    document.body.appendChild( scriptElement );
})();
