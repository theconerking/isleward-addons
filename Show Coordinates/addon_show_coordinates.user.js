// ==UserScript==
// @name         Isleward - Show Coordinates
// @namespace    Isleward.Addon
// @version      1.0.0
// @description  Displays the Coordinates of the Player below the clock
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==


(function () {
    var scriptElement = document.createElement( "script" );
    scriptElement.type = "text/javascript";
    scriptElement.src = "https://cdn.statically.io/gl/islewardtribrid/isleward-addons/raw/main/Show%20Coordinates/addon_show_coordinates.js";
    document.body.appendChild( scriptElement );
})();
