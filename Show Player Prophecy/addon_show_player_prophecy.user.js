// ==UserScript==
// @name         Isleward - Show Player Prophecies
// @namespace    Isleward.Addon
// @version      1.0.0
// @description  Displays the Prophecies of players, including your own prophecy
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==


(function () {
    var scriptElement = document.createElement( "script" );
    scriptElement.type = "text/javascript";
    scriptElement.src = "https://cdn.statically.io/gl/islewardtribrid/isleward-addons/raw/main/Show%20Player%20Prophecy/addon_show_player_prophecy.js";
    document.body.appendChild( scriptElement );
})();


