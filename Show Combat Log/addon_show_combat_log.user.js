// ==UserScript==
// @name         Isleward - Show Combat Log
// @namespace    Isleward.Addon
// @version      1.0.0
// @description  Shows all information related to combat in the chat
// @author       Initially created by Polfy, edited by Tribrid
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==


(function () {
    var scriptElement = document.createElement( "script" );
    scriptElement.type = "text/javascript";
    scriptElement.src = "https://cdn.statically.io/gl/islewardtribrid/isleward-addons/raw/main/Show%20Combat%20Log/addon_show_combat_log.js";
    document.body.appendChild( scriptElement );
})();
