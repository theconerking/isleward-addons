// ==UserScript==
// @name         Isleward - Show Combat Log
// @namespace    Isleward.Addon
// @version      1.0.0
// @description  Shows all information related to combat in the chat
// @author       Initially created by Polfy, edited by Tribrid
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==


var idToName = {};
var inCombatWith = {};

addons.register({
    init: function(events) {
        events.on('onGetObject', this.onGetObject.bind(this));
        events.on('onGetDamage',this.onGetDamage.bind(this));
    },

    onGetObject: function(obj) {
        if(obj.name !== undefined){
            idToName[obj.id]=obj.name;
        }
        if (typeof window.showCombatLog !== 'undefined') {
            if(window.showCombatLog == false ) {
                return;
            }
        }
            if(obj.destroyed !== undefined && obj.destroyed == true){
                if(obj.id in inCombatWith){
                    setTimeout(function(){addCombatMessage(idToName[obj.id] + " has been killed/went out of range", "redA")}, 100);
                    delete inCombatWith[obj.id];
                }
            }
        
    },

    onGetDamage: function(dmg) {
        if (typeof window.showCombatLog !== 'undefined') {
            if(window.showCombatLog == false ) {
                return;
            }
        }
            if(dmg.crit !== undefined){
                if(dmg.id !== undefined && dmg.source !== undefined){
                    var enemyName;
                    var action="hit";
                    if(dmg.heal !== undefined && dmg.heal == true){
                        action="heal";
                    }
                    if(window.player !== undefined && dmg.source == window.player.id){
                        inCombatWith[dmg.id] = true;
                        enemyName = idToName[dmg.id];
                        addCombatMessage("You "+(dmg.crit == true ? "critically ":"")+action+" "+enemyName+" for "+ (~~dmg.amount) +" damage", "blueA");
                    } else if(window.player !== undefined && dmg.id == window.player.id){
                        enemyName = idToName[dmg.source];
                        inCombatWith[dmg.source] = true;
                        addCombatMessage(enemyName+(dmg.crit == true ? " critically":"")+" "+action+"s you for "+ (~~dmg.amount) +" damage", "tealC");
                    }
                }
            } else{
                if(dmg.event !== undefined){
                    if(window.player !== undefined && dmg.id == window.player.id && dmg.text.indexOf(" xp") != -1){
                        setTimeout(function(){addCombatMessage("You gained "+dmg.text, "redA")}, 200);
                    }
                }
            }
    }
});


function addCombatMessage(txt, colortxt){
    jQuery('<div class="list-message color-'+colortxt+' chat CombatLog">' + txt + '</div>').appendTo(jQuery(".uiMessages .list"));
    jQuery(".uiMessages .list").scrollTop(9999999);
}
