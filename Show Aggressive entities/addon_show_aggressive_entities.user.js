// ==UserScript==
// @name         Isleward - Show Aggressive Enemies
// @namespace    Isleward.Addon
// @version      1.0.0
// @description  Displays whether or not a enemy is aggressive
// @author       Tribrid 
// @match        https://play.isleward.com/
// @grant        none
// ==/UserScript==


(function () {
    var scriptElement = document.createElement( "script" );
    scriptElement.type = "text/javascript";
    scriptElement.src = "https://cdn.statically.io/gl/islewardtribrid/isleward-addons/raw/main/Show%20Aggressive%20entities/addon_show_aggressive_entities.js";
    document.body.appendChild( scriptElement );
})();
